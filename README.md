# Docker images

Docker images for the project.

- [mongodb:4.4-bionic-rs](https://gitlab.com/food_project/docker/-/tree/master/mongodb/4.4-replica-set) - test image of mongo with replica set initialization
- [fluentd:v1.12-1](https://gitlab.com/food_project/docker/-/tree/master/fluentd/) - fluentd with elasticsearch plugin