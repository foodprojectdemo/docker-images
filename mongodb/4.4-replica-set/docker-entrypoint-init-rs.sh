#!/bin/bash
set -e

function waitForMongo {
    n=0
    until [ $n -ge 20 ]
    do
        mongo admin --quiet --eval "db" && break
        n=$[$n+1]
        sleep 5
    done
}

eval "docker-entrypoint.sh $@" &
PID=$!

echo "PID $PID"

sleep 10

echo "waitForMongo"
waitForMongo
echo "waitForMongoEnd"

mongo -u "$MONGO_INITDB_ROOT_USERNAME" --authenticationDatabase admin -p "$MONGO_INITDB_ROOT_PASSWORD" --eval "rs.initiate();"

unset "${!MONGO_INITDB_@}"

wait $PID
